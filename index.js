const { config } = require("dotenv");

// Load environment variables from .env file
config();

const FileUpdater = require("./utils/FileUpdater");
const OSSUploader = require("./utils/OSSUploader");

const directoryPath = "./templates";
const searchValue = "%MEDIA_URL%";
const replaceValue = process.env.MEDIA_URL;
const targetBucketDirectory = "email_templates";

FileUpdater.replaceInFiles(directoryPath, searchValue, replaceValue)
  .then(async () => {
    console.log("All files updated successfully.");

    const uploader = new OSSUploader(
      process.env.OSS_ACCESS_KEY_ID,
      process.env.OSS_ACCESS_SECRET_KEY,
      process.env.OSS_REGION,
      process.env.OSS_BUCKET
    );

    await uploader.uploadAndVerifyFiles(directoryPath, targetBucketDirectory);
  })
  .catch((error) => {
    console.error("Error:", error);
  });
