const fs = require('fs');
const path = require('path');

class FileUpdater {
  static replaceInFiles(directoryPath, searchValue, replaceValue) {
    return new Promise((resolve, reject) => {
      fs.readdir(directoryPath, (err, files) => {
        if (err) {
          reject(`Error reading directory ${directoryPath}: ${err}`);
          return;
        }

        files.forEach((file) => {
          const filePath = path.join(directoryPath, file);
          fs.stat(filePath, (err, stats) => {
            if (err) {
              reject(`Error stating file ${filePath}: ${err}`);
              return;
            }

            if (stats.isDirectory()) {
              // Recursively call replaceInFiles for subdirectories
              FileUpdater.replaceInFiles(filePath, searchValue, replaceValue)
                .then(() => resolve())
                .catch(reject);
            } else if (stats.isFile()) {
              // Replace occurrences in files
              fs.readFile(filePath, 'utf8', (err, data) => {
                if (err) {
                  reject(`Error reading file ${filePath}: ${err}`);
                  return;
                }

                const replacedData = data.replace(
                  new RegExp(searchValue, 'g'),
                  replaceValue
                );

                fs.writeFile(filePath, replacedData, 'utf8', (err) => {
                  if (err) {
                    reject(`Error writing file ${filePath}: ${err}`);
                    return;
                  }
                  console.log(`File '${filePath}' updated successfully.`);
                  resolve();
                });
              });
            }
          });
        });
      });
    });
  }
}

module.exports = FileUpdater;
