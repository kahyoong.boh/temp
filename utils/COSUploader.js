const fs = require('fs');
const path = require('path');

// Log in to https://console.tencentcloud.com/cam/capi to check and manage the SecretId and SecretKey of your project.
const COS = require('cos-nodejs-sdk-v5');

class COSUploader {
  constructor(secretId, secretKey, region, bucket, storageClass) {
    // Initialize the OSS client. Replace the following parameters with your actual configuration information.
    this.client = new COS({
      SecretId: secretId, // User `SecretId`. We recommend you obtain it from the environment variable. In addition, we recommended you use a sub-account key and follow the principle of least privilege to reduce risks. For information about how to obtain a sub-account key, visit https://cloud.tencent.com/document/product/598/37140.
      SecretKey: secretKey, // User `SecretKey`. We recommend you obtain it from the environment variable. In addition, we recommend you use a sub-account key and follow the principle of least privilege to reduce risks. For information about how to obtain a sub-account key, visit https://cloud.tencent.com/document/product/598/37140.
    });

    this.region = region;
    this.bucket = bucket;
    this.storageClass = storageClass ?? 'STANDARD'; // MAZ_STANDARD, MAZ_STANDARD_IA, MAZ_INTELLIGENT TIERING, INTELLIGENT TIERING, STANDARD, STANDARD_IA, ARCHIVE, and DEEP ARCHIVE
  }

  async uploadAndVerifyFiles(
    baseDirectory,
    storageBaseDirectory,
    relativeDirectory = ''
  ) {
    try {
      // Read the contents of the directory
      const files = fs.readdirSync(path.join(baseDirectory, relativeDirectory));

      for (const file of files) {
        const filePath = path.join(baseDirectory, relativeDirectory, file);
        const stats = fs.statSync(filePath);

        if (stats.isDirectory()) {
          // Recursively call uploadAndVerifyFiles for subdirectories
          await this.uploadAndVerifyFiles(
            baseDirectory,
            storageBaseDirectory,
            path.join(relativeDirectory, file)
          );
        } else if (stats.isFile()) {
          // Upload the file to Cloud Storage with the correct directory structure
          const objectName = `${storageBaseDirectory}/${relativeDirectory}/${file}`;
          const uploadResult = await this.client.putObject(
            {
              Bucket: this.bucket,
              Region: this.region,
              Key: objectName,
              StorageClass: this.storageClass,
              Body: fs.createReadStream(filePath), // The target file object to be uploaded
              onProgress: (progressData) => {
                console.log(JSON.stringify(progressData));
              },
            },
            // Callback
            (err, data) => {
              console.log(err || data);
            }
          );
          console.log(`Upload succeeded for ${objectName}:`, uploadResult);
        }
      }
    } catch (error) {
      console.error('Error:', error);
      // Write your error handling code here.
    }
  }
}

module.exports = COSUploader;
