const fs = require('fs');
const path = require('path');
const OSS = require('ali-oss');

class OSSUploader {
  constructor(keyId, keySecret, region, bucket) {
    // Initialize the OSS client. Replace the following parameters with your actual configuration information.
    this.client = new OSS({
      accessKeyId: keyId, // Make sure that the OSS_ACCESS_KEY_ID environment variable is configured.
      accessKeySecret: keySecret, // Make sure that the OSS_ACCESS_KEY_SECRET environment variable is configured.
      region: region, // Specify the region in which the bucket resides. Example: 'oss-cn-hangzhou'.
      bucket: bucket, // Specify the name of the bucket. Example: 'my-bucket-name'.
    });
  }

  async uploadAndVerifyFiles(
    baseDirectory,
    targetOSSDirectory,
    relativeDirectory = ''
  ) {
    try {
      // Read the contents of the directory
      const files = fs.readdirSync(path.join(baseDirectory, relativeDirectory));

      for (const file of files) {
        const filePath = path.join(baseDirectory, relativeDirectory, file);
        const stats = fs.statSync(filePath);

        if (stats.isDirectory()) {
          // Recursively call uploadAndVerifyFiles for subdirectories
          await this.uploadAndVerifyFiles(
            baseDirectory,
            targetOSSDirectory,
            path.join(relativeDirectory, file)
          );
        } else if (stats.isFile()) {
          // Upload the file to OSS with the correct directory structure
          const objectName = `${targetOSSDirectory}/${relativeDirectory}/${file}`;
          const uploadResult = await this.client.put(objectName, filePath);
          console.log(`Upload succeeded for ${objectName}:`, uploadResult);

          // Download the object from OSS to verify that the upload was successful.
          const getResult = await this.client.get(objectName);
          console.log('Object downloaded:', getResult);
        }
      }
    } catch (error) {
      console.error('Error:', error);
      // Write your error handling code here.
    }
  }
}

module.exports = OSSUploader;
